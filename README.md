# Coronavirus - Predictions with sigmoid

This Machine Learning project aims to generalize deaths caused by the coronavirus in order to **predict the end of the pandemic**.

The algorithm implemented in this project doesn't use [SEIR and SEIRS models](https://www.idmod.org/docs/hiv/model-seir.html).
It simply consists to interpolate all the measurements given every day by a **sigmoid** curve.

The goal of this project is to validate the intuition that a simple mathematical equation with a **S** shape is enough to explain the COVID-19 spread for each country around the world.

In nature, lot of phenomena can be explained by a sigmoid curve, because of it start slowly, accelerate and reach a maximum differential value, then decelerate to finally disappear.
If this intuition turns out to be verified, then it's not necessary to use deep learning and others expensive algorithms which need lot of data.
Furthermore, the volume of data used here isn't really big...

Through all the tests carried out in this project, the sigmoid's shape has successfully generalized all the measurement points to date !

The predictions below have been performed by using data **from France** (based on the total number of deaths in hospitals).

Note that this project can be customized with data from other countries.

If you want to understand how it works, please check the ![JupyterLab notebook](./notebook/Coronavirus.ipynb),
the ![SigmoidRegression library](./lib/SigmoidRegression.py) and the configuration for ![France](./country/fr).

# 1 - Requirements

## Python 3
Please install Anaconda (for Python 3 and JupyteLab).

## Plotly V4.6.0
```shell
pip install plotly==4.6.0
```

## Spark V2.x (recommended)
The **SigmoidRegression** algorithm can be used with Spark in order to reduce significantly the computation time.
You can install Spark like this:
```shell
pip install pyspark
```

# 2 - Algorithm's description

During the training step related on the sigmoid regression (see the equation below), the minimal cost (root mean square or RMS) corresponds to the best parameter values for **a** (the amplitude), **l** (the lambda) and **s** (the shift):

<img src="./img/Sigmoid-main-equation.png" width="188" height="71"><br>

The implemented approach used here is not based on gradient-descent.
It's rather an optimization problem where the algorithm must try all the combinations of parameters in order to find the best solution (cf. NP-complete problems).

It's possible to choose between 2 types of calculations:

- A **mono-core** calculation which uses a **pure Python** code and spends lot of time.
    - **Eg**: With 16608321 combinations of parameters and 42 rows => **Time spent = 1h 11 min 15 s** (by using 1 x 2.4 GHz CPU)
- A **multi-cores** calculation which uses a really fast parallelization with **PySpark**.
    - **Eg**: With 16608321 combinations of parameters and 42 rows => **Time spent = 2 min 2 s** (by using 32 x 2.4 GHz CPUs)

# 3 - Predictions made for France

Please check the ![diagrams](./country/fr/diagrams), the ![data and the configuration](./country/fr).

The end of pandemic for (max death) is explained by the absence of variation on the **blue** sigmoid curve.

Let’s analyze the **end of the pandemic** forecast:
- On 2020-05-06 (day 65):<br>
                          **End for (max death=16242) = 2020-06-08** (day 98)<br>
                          **End for (max death - 50) = 2020-05-23** (day 82)<br>
                          **End for (max death - 100) = 2020-05-18** (day 77)
- On 2020-05-05 (day 64): End for (max death=16216) = 2020-06-08 (day 98) -> 0 % error (0 days) against day 65.
- On 2020-05-04 (day 63): End for (max death=15632) = 2020-06-04 (day 94) -> -4.08 % error (4 days) against day 65.
- On 2020-04-27 (day 56): End for (max death=15414) = 2020-06-04 (day 94) -> -4.08 % error (4 days) against day 65.
- On 2020-04-25 (day 54): End for (max death=14686) = 2020-05-30 (day 89) -> -9.18 % error (-9 days) against day 65.
- On 2020-04-22 (day 51): End for (max death=13850) = 2020-05-28 (day 87) -> -11.22 % error (-11 days) against day 65.
- On 2020-04-19 (day 48): End for (max death=12962) = 2020-05-22 (day 81) -> -17.35 % error (-17 days) against day 65.
- On 2020-04-16 (day 45): End for (max death=12047) = 2020-05-19 (day 78) -> -20.41 % error (-20 days) against day 65.
- On 2020-04-12 (day 41): End for (max death=11091) = 2020-05-15 (day 74) -> -24.49 % error (-24 days) against day 65.
- On 2020-04-01 (day 30): End for (max death=9088) = 2020-05-09 (day 68) -> -30.61 % error (-30 days) against day 65.
- On 2020-03-27 (day 25): End for (max death=9038) = 2020-05-04 (day 63) -> -35.71 % error (-35 days) against day 65.

## Prediction made on 2020-05-06
![COVID-19 France prediction](./country/fr/diagrams/Covid19_France-Predictions-2020_05_06.png)

## Prediction made on 2020-04-25
![COVID-19 France prediction](./country/fr/diagrams/Covid19_France-Predictions-2020_04_25.png)

## Prediction made on 2020-04-16
![COVID-19 France prediction](./country/fr/diagrams/Covid19_France-Predictions-2020_04_16.png)

## Prediction made on 2020-04-11
![COVID-19 France prediction](./country/fr/diagrams/Covid19_France-Predictions-2020_04_11.png)

## Prediction made on 2020-04-01
![COVID-19 France prediction](./country/fr/diagrams/Covid19_France-Predictions-2020_04_01.png)

## Prediction made on 2020-03-27
![COVID-19 France prediction](./country/fr/diagrams/Covid19_France-Predictions-2020_03_27.png)

# 4 - Maths reminder
## Sigmoid equations
**Description of equation's parameters:**
- **a**: the amplitude
- **l**: the lambda
- **s**: the shift
- **d**: the delta (clipping values)
- **p**: the period (between the two deltas)
<img src="./img/Sigmoid-all-equations.png" width="755" height="421">

Please use [Desmos](https://www.desmos.com) if you want to simulate the equations above.

## Sigmoid Taylor series (just for fun and curiosity)
<img src="./img/Sigmoid-Taylor_series.png">

Tayor series generated by [CodaBrainy](https://www.codabrainy.com/taylor)
