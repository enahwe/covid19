#!/bin/python
# -*- coding: utf-8 -*-

###########################
# SigmoidRegression class #
###########################
# Developped by Philippe ROSSIGNOL (2020-04-01)
class SigmoidRegression:
  
    import math
    import numpy
    import pandas as pd

    config = None

    dfIn = None
    dfOut = None

    X = None
    Y = None
    Amplitude = None
    Lambda = None
    Shift = None
    
    spark = None
    sc = None

    def __init__(self, spark, configFile, dataFile):
        self.spark = spark
        if (self.spark != None): self.sc = self.spark.sparkContext
        self.config = self._loadPropertiesFile(configFile)
        self.dfIn = self.pd.read_csv(dataFile, delimiter=';')
        self.X = self.dfIn[self.config.get("xColName")].to_list()
        self.Y = self.dfIn[self.config.get("yColName")].to_list()
        if (self.config.get("sparkEnabled")):
            if (self.spark == None): self.sc, self.spark = self._createSparkContextAndSession()
            if (self.sc != None): self.sc.addPyFile(self._getObjectClassFilePath(self))

    def getSigmoidY(self, x, Amplitude=None, Lambda=None, Shift=None):
        if (Amplitude == None): Amplitude = self.Amplitude
        if (Lambda == None): Lambda = self.Lambda
        if (Shift == None): Shift = self.Shift
        y = Amplitude / (1 + self.math.exp(-Lambda * (x - Shift)))
        return y

    def getSigmoidX(self, y, Amplitude=None, Lambda=None, Shift=None):
        if (Amplitude == None): Amplitude = self.Amplitude
        if (Lambda == None): Lambda = self.Lambda
        if (Shift == None): Shift = self.Shift
        x = Shift - self.math.log(Amplitude / y - 1) / Lambda
        return x

    def getSigmoidShift(self, x=None, y=None, Amplitude=None, Lambda=None):
        if (x == None): x = 0
        if (y == None): y = self.measures[x]
        if (Amplitude == None): Amplitude = self.Amplitude
        if (Lambda == None): Lambda = self.Lambda
        Shift = x + self.math.log(Amplitude / y - 1) / Lambda
        return Shift

    def fit(self):
        print(self._getCurrentDt() + " - Start")

        # Build the amplitude range
        amplitude_min = self.config.get("amplitude_min")
        amplitude_max = self.config.get("amplitude_max")
        amplitude_step = self.config.get("amplitude_step")
        a_range = self.numpy.arange(amplitude_min, amplitude_max + amplitude_step, amplitude_step)

        # Build the lambda range
        lambda_min = self.config.get("lambda_min")
        lambda_max = self.config.get("lambda_max")
        lambda_step = self.config.get("lambda_step")
        l_range = self.numpy.arange(lambda_min, lambda_max + lambda_step, lambda_step)

        # Build the shift range
        shift_min = self.config.get("shift_min")
        shift_max = self.config.get("shift_max")
        shift_step = self.config.get("shift_step")
        s_range = self.numpy.arange(shift_min, shift_max + shift_step, shift_step)

        # Number of combinations
        nbrCombinations = len(a_range) * len(l_range) * len(s_range)
        print(self._getCurrentDt() + " - Number of parameter combinations = " + str(nbrCombinations))

        # Init parameters
        self.Amplitude = 0
        self.Lambda = 0
        self.Shift = 0

        # Multicores training with Spark
        if (self.config.get("sparkEnabled")):
            if (self.spark != None):
                self._multiCoresTrain(self.spark, self.X, self.Y, a_range, l_range, s_range)
        # Monocore training
        else:
            self._monoCoreTrain(self.X, self.Y, a_range, l_range, s_range)

        # Print the optimal parameters
        print(self._getCurrentDt() + " - Amplitude = " + str(round(self.Amplitude, 4)))
        print(self._getCurrentDt() + " - Lambda = " + str(round(self.Lambda, 4)))
        print(self._getCurrentDt() + " - Shift = " + str(round(self.Shift, 4)))

        self.dfOut = None

        print(self._getCurrentDt() + " - End")

    def showEquation(self, varName="y", Amplitude=None, Lambda=None, Shift=None):
        from IPython.display import Markdown, display
        if (Amplitude == None): Amplitude = self.Amplitude
        if (Lambda == None): Lambda = self.Lambda
        if (Shift == None): Shift = self.Shift
        y = "\\begin{equation*}"
        y += varName + "=\\frac{" + str(Amplitude) + "}{1+\\ e^{-" + str(round(Lambda, 4)) + " (x-" + str(round(Shift, 4)) + ")}}"
        y += "\\end{equation*}"
        display(Markdown(y))

    def showEquations(self):
        from IPython.display import Markdown, display
        y = "\\begin{equation*}"
        y += "y=\\frac{a}{1+\\ e^{-l(x-s)}}"
        y += "\\end{equation*}"
        display(Markdown(y))
        print()

        l = "\\begin{equation*}"
        l += "l=\\frac{1}{s-x}.\\ ln(\\frac{a-y}{y})"
        l += "\\end{equation*}"
        display(Markdown(l))
        print()

        s = "\\begin{equation*}"
        s += "s=x+\\frac{1}{l}.\\ ln(\\frac{a-y}{y})"
        s += "\\end{equation*}"
        display(Markdown(s))
        print()

        s = "\\begin{equation*}"
        s += "x=s-\\frac{1}{l}.\\ ln(\\frac{a-y}{y})"
        s += "\\end{equation*}"
        display(Markdown(s))
        print()

        dx1 = "\\begin{equation*}"
        dx1 += "p=-\\frac{1}{l}.[\\ ln(\\frac{a-y_1}{y_1}) - \\ ln(\\frac{a-y_0}{y_0})]"
        dx1 += "\\end{equation*}"
        display(Markdown(dx1))
        print()

        dx2 = "\\begin{equation*}"
        dx2 += "p=\\frac{2}{l}.\\ ln(\\frac{a-d}{d})"
        dx2 += "\\end{equation*}"
        display(Markdown(dx2))
        print()

        d = "\\begin{equation*}"
        d += "d=\\frac{a}{1+\\ e^{-l(\\frac{p}{2})}}"
        d += "\\end{equation*}"
        display(Markdown(d))
        print()
    
    def getDf(self, xMax):
        try:
            if (self.dfOut.empty): raise()
        except BaseException as err:
            self.dfOut = self._buildDf(xMax)
        return self.dfOut

    def getFigure(self,
                  xMax,
                  title=None,
                  labelPred="Prediction",
                  labelPredDiff="Prediction diff",
                  labelData="Measure",
                  labelDataDiff="Measure diff",
                  height=500,
                  cartridge=True):
        # Get the dataframe containing the predictions
        df = self.getDf(xMax)
        # Create the Plotly figure
        if (title == None): title=""
        import plotly.express as px
        fig = px.line(title=title, height=height)
        fig.add_scatter(x=df["X"], y=df["Y_pred"], mode="markers+lines", name=labelPred)
        fig.add_scatter(x=df["X"], y=df["Y_pred_diff"], mode="markers+lines", name=labelPredDiff)
        fig.add_scatter(x=df["X"], y=df["Y_real"], mode="markers+lines", name=labelData)
        fig.add_scatter(x=df["X"], y=df["Y_real_diff"], mode="markers+lines", name=labelDataDiff)
        # Add parameters info
        if (cartridge):
            import plotly.graph_objects as go
            fig.add_annotation(go.layout.Annotation(
                    text = "- Sigmoid parameters:" +
                           "<br>Amplitude = " + str(self.Amplitude) + 
                           "<br>Lambda = " + str(round(self.Lambda, 3)) +
                           "<br>Shift = " + str(round(self.Shift, 3)),
                    align='left',
                    showarrow=False,
                    xref='paper',
                    yref='paper',
                    x=1.195,
                    y=0,
                    bordercolor='black',
                    borderwidth=1
            ))
        return fig

    # =====================
    # = PRIVATE FUNCTIONS =
    # =====================

    def _monoCoreTrain(self, X, Y, A_range, L_range, S_range):
        cpt = 0
        globalCost = self.math.inf
        Amplitude = 0
        Lambda = 0
        Shift = 0
        nbrLoops = len(A_range) * len(L_range) * len(S_range)
        print("0 % - Cost = " + str(globalCost))
        for a in A_range:
            for l in L_range:
                for s in S_range:
                    cost = 0
                    for i, y_real in enumerate(Y):
                        y_pred = self.getSigmoidY(X[i], Amplitude=a, Lambda=l, Shift=s)
                        cost += pow(y_pred - y_real, 2)
                    if (cost < globalCost):
                        globalCost = cost
                        Amplitude = a
                        Lambda = l
                        Shift = s
                    cpt+=1
                    if ((cpt % int(nbrLoops/10))==0):
                        percent = round(100*cpt/nbrLoops, 1)
                        print(str(percent) + "% - Cost = " + str(round(globalCost, 4)) +
                              " (Amplitude=" + str(round(Amplitude, 4)) +
                              ", Lambda=" + str(round(Lambda, 4)) +
                              ", Shift=" + str(round(Shift, 4)) + ")")
        self.Amplitude = Amplitude
        self.Lambda = Lambda
        self.Shift = Shift

    def _multiCoresTrain(self, sparkSession, X, Y, a_range, l_range, s_range):
        # Creation and spreading of rows to all cores via Spark
        rows = []
        for a in a_range:
            for l in l_range:
                for s in s_range:
                    rows.append((float(a), float(l), float(s)))
        nbrRows = len(rows)
        print(self._getCurrentDt() + " - Local data successfully created (number of rows = " + str(nbrRows) + ")")
        print(self._getCurrentDt() + " - Disseminate data to all cores with Spark")
        df = self.sc.parallelize(rows).toDF(["A", "L", "S"])
        print(self._getCurrentDt() + " - Data successfully disseminated")
        # Show the number of partitions
        nbrPartitions = df.rdd.getNumPartitions()
        print(self._getCurrentDt() + " - Number of partitions = " + str(nbrPartitions))
        minPartitions = self.config.get("minPartitions")
        maxPartitions = self.config.get("maxPartitions")
        if (nbrPartitions < minPartitions or nbrPartitions > maxPartitions):
            repartition = self.config.get("repartition")
            print(self._getCurrentDt() + " - Repartition to " + str(repartition))
            df = df.repartition(repartition)
            nbrPartitions = df.rdd.getNumPartitions()
            print(self._getCurrentDt() + " - Number of partitions = " + str(nbrPartitions))
        # Computing all the costs related on the number of combinations
        print(self._getCurrentDt() + " - Calculating the " + str(nbrRows) + " costs and searching the minimal cost")
        cost = Cost()
        cost.X = X # X array will be serialized with the object cost
        cost.Y = Y # Y array will be serialized with the object cost
        df = self._createColumnWithRule(df, "C", "float", cost, "getCost", columns=["A", "L", "S"])
        from pyspark.sql import functions as F
        df = df.cache()
        dfr = df.agg(F.min("C"))
        minCost = dfr.first().asDict().get("min(C)")
        # Searching the row corresponding to the minimal cost
        print(self._getCurrentDt() + " - Searching the row corresponding to the minimal cost")
        dfr = df.where("C=" + str(minCost))
        resultRow = dfr.first().asDict()
        # Unpersist the dataframe
        df.unpersist()
        # Set the optimal parameters
        self.Amplitude = resultRow.get("A")
        self.Lambda = resultRow.get("L")
        self.Shift = resultRow.get("S")

    def _buildDf(self, xMax):
        import pandas as pd
        # X range
        X = range(0, xMax)
        # Real Y values
        Y_real = self.Y.copy()
        for x in range(0, xMax):
            if (x>=len(self.Y)): Y_real.append(None)
        # Real Y diff
        Y_real_diff = []
        for x in range(0, xMax):
            if (x == 0): Y_real_diff.append(0)
            else:
                if (Y_real[x] != None): Y_real_diff.append(Y_real[x] - Y_real[x-1])
                else: Y_real_diff.append(None)
        # Predictive Y values
        Y_pred = []
        for x in range(0, xMax):
            Y_pred.append(round(self.getSigmoidY(x)))
        # Predictive Y diff
        Y_pred_diff = []
        for x in range(0, xMax):
            if (x == 0): Y_pred_diff.append(0)
            else: Y_pred_diff.append(Y_pred[x] - Y_pred[x-1])
        # Build the dataframe
        cols = {
            "X": X,
            "Y_real": Y_real,
            "Y_real_diff": Y_real_diff,
            "Y_pred": Y_pred,
            "Y_pred_diff": Y_pred_diff
        }
        df = pd.DataFrame(cols, columns = ["X", "Y_real", "Y_real_diff", "Y_pred", "Y_pred_diff"])
        self.dfOut = df
        return df

    def _getCurrentDt(self):
        import datetime, time
        currentDt = datetime.datetime.now()
        dtStr = currentDt.strftime("%Y-%m-%d %H:%M:%S")
        return dtStr

    def _loadPropertiesFile(self, configFile):
        def _getFileContent(aFilePath):
            aFile = open(aFilePath, "r")
            aContent = aFile.read()
            aFile.close()
            return aContent
        conf = {}
        content = _getFileContent(configFile)
        content = content.replace('\r', '\n')
        content = content.replace('\\\n', '')
        lines = content.split('\n')
        for line in lines:
            if (not line.strip().startswith('#') and line.strip() != '' and '=' in line):
                evaluate = False
                key, value = line.split('=', 1)        
                key = key.strip()
                if ((value.strip().startswith('"') and value.strip().endswith('"')) or
                    (value.strip().startswith("'") and value.strip().endswith("'"))):
                    value = value.strip()[1:-1]
                else:
                    value = value.strip()
                    evaluate = True
                value = value.replace('\\"', '"')
                if (evaluate):
                    try:
                        value = eval(value)
                    except BaseException as err:
                        pass
                conf.update( {key : value} )
        return conf

    def _createColumnWithRule(self, df, columnName, columnType, ruleObject, ruleName, columns=None):
        from pyspark.sql import functions as F
        from pyspark.sql import types as T
        def _getSparkClassType(shortType):
            defaultSparkClassType = "StringType"
            typesMapping = {
                "bigint"    : "LongType",
                "binary"    : "BinaryType",
                "boolean"   : "BooleanType",
                "byte"      : "ByteType",
                "date"      : "DateType",
                "decimal"   : "DecimalType",
                "double"    : "DoubleType",
                "float"     : "FloatType",
                "int"       : "IntegerType",
                "integer"   : "IntegerType",
                "long"      : "LongType",
                "numeric"   : "NumericType",
                "string"    : defaultSparkClassType,
                "timestamp" : "TimestampType"
            }
            sparkClassType = None
            try:
                sparkClassType = typesMapping[shortType]
            except:
                sparkClassType = defaultSparkClassType
            return sparkClassType
        if (columnType != None): sparkClassType = _getSparkClassType(columnType)
        else: sparkClassType = "StringType"
        aUdf = eval("F.udf(ruleObject." + ruleName + ", T." + sparkClassType + "())")
        if (columns != None):
            structColumns = F.struct([df[column] for column in columns])
            df = df.withColumn(columnName, aUdf(structColumns))
        else:
            df = df.withColumn(columnName, aUdf(F.lit(None)))
        # Return a dataframe
        return df

    def _getObjectClassFilePath(self, anObject):
        import sys, os
        filePath = os.path.abspath(sys.modules[anObject.__class__.__module__].__file__)
        return filePath

    def _createSparkContextAndSession(self):
        sc = None
        spark = None
        try:
            from pyspark.sql import SparkSession
            builder = SparkSession.builder
            builder = builder.appName("Sigmoid")
            builder = builder.master(self.config.get("sparkMaster"))
            sparkConf = self.config.get("sparkConf")
            for key in sparkConf: builder = builder.config(key, sparkConf[key])
            spark = builder.getOrCreate()
            sc = spark.sparkContext
        except BaseException as err:
            err = str(err)
            spark = None
            sc = None
            print("- SPARK ERROR: " + err)
        return sc, spark            

class Cost:
    X = None
    Y = None
    def getCost(self, columns=None):
        import math
        def getSigmoidY(a, l, s, x):
            y = a / (1 + math.exp(-l * (x - s)))
            return y
        a = columns[0]; l = columns[1]; s = columns[2]
        cost = 0
        for i, y_real in enumerate(self.Y):
            y_pred = getSigmoidY(a, l, s, self.X[i])
            cost += pow(y_pred - y_real, 2)
        return cost
